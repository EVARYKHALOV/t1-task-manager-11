package ru.t1.varykhalov.tm.api;

public interface ICommandController {

    void showInfo();

    void showArgumentError();

    void showCommandError();

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void exit();

}